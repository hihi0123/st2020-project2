const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch( {headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
	const browser = await puppeteer.launch( {headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(title).toBe('Join');
    await browser.close();

})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
	const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto('http://localhost:3000/chat.html?name=123456&room=123456');
    await page.screenshot({path: 'test/screenshots/Welcome.png'});
    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
	const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto('http://localhost:3000/chat.html?name=&room=');
    await page.screenshot({path: 'test/screenshots/Error.png'});
    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
	const browser = await puppeteer.launch( {headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto('http://localhost:3000/chat.html?name=ooo&room=R16');
    await page.keyboard.press('Enter', {delay: 500});
    let title = await page.$eval('#messages > li > div:nth-child(2) > p', (content) => content.innerHTML);
    expect(title).toBe('Hi ooo, Welcome to the chat app');
    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
	const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', 100);
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('R1');

    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
	const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Obama', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('Obama');
    
    await browser.close();
})

// 11
test('[Content] The "Send" button should exist', async () => {
	const browser = await puppeteer.launch( {headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto('http://localhost:3000/chat.html?name=000&room=000');
    let title = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(title).toBe('Send location');
    await browser.close();

})

// 12
test('[Behavior] Send a message', async () => {
	const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto('http://localhost:3000/chat.html?name=nobody&room=R87');
    await page.keyboard.press('Enter', {delay: 1000});
    await page.type('#message-form > input[type=text]', 'hi', {delay: 1000});
    //await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#messages > li:nth-child(2) > div.message__body > p');    
    let member = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div.message__body > p').innerHTML;
    });

    expect(member).toBe('hi');
    
    await browser.close();

	/*const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto('http://localhost:3000/chat.html?name=nobody&room=R87');

    await page.keyboard.press('Enter', {delay: 1000});

    await page.type('#message-form > input[type=text]', 'o', {delay: 1000});
    
    await page.waitForSelector('#message-form > input[type=text]');
    let userName = await page.evaluate(() => {
        return document.querySelector('#message-form > input[type=text]').innerHTML;
    });
    expect(userName).toBe('o');

    await page.keyboard.press('Enter', {delay: 1000});
    
    

    await browser.close();*/


})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto('http://localhost:3000/chat.html?name=John&room=R66');
    await page.keyboard.press('Enter', {delay: 100});
    await page.type('#message-form > input[type=text]', 'hi', {delay: 100});
    //await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.keyboard.press('Enter', {delay: 100}); 

    const page2 = await browser.newPage();

    await page2.goto('http://localhost:3000/chat.html?name=Mike&room=R66');
    await page2.keyboard.press('Enter', {delay: 100});
    await page2.type('#message-form > input[type=text]', 'hello', {delay: 100});
    //await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 
    await page2.keyboard.press('Enter', {delay: 100}); 

    await browser.close();

    /*const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto('http://localhost:3000/chat.html?name=John&room=R66');
    await page.type('#message-form > input[type=text]', 'Hi', 100);
    
    const page2 = await browser.newPage();
    await page2.goto('http://localhost:3000/chat.html?name=Mike&room=R66');
    await page2.type('#message-form > input[type=text]', 'Hello', 100);
    
    await browser.close();*/
})

// 14
test('[Content] The "Send location" button should exist', async () => {
	const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto('http://localhost:3000//chat.html?name=nobody&room=R35');
    let title = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(title).toBe('Send location');
    await browser.close();


})

// 15
test('[Behavior] Send a location message', async () => {
	const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
   
    await page.goto('http://localhost:3000//chat.html?name=forMap&room=R99');
    await page.keyboard.press('Enter', {delay: 500});
    //await page.keyboard.press('Enter', {delay: 1000});

    //await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    //await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    //await page.mouse.click('Enter', {delay: 100}); 
    //await page.$eval('#send-location',elem=>elem.click());
    //await page.click("#send-location")
    
    await page.$eval('#send-location',elem=>elem.click()); // 一個"$"字符表示只會選取一個元素，如果是用"$$"則表示目標元素有兩個以上
	//await test_variable.click()

    await browser.close();


})
